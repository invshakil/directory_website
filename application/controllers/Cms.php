<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cms extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('crud_model');
        $this->load->database();
        $this->load->library('session');

        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'admin_login', 'refresh');

    }

    function index()
    {
        $data = array();
        $data['page_name'] = 'Admin Dashboard';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/dashboard_content', $data, true);

        return $this->load->view('backend/index', $data);
    }

    /** Image Upload **/
    function do_upload()
    {

        $this->load->library('image_lib');
        $config['upload_path'] = './logo_upload/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 250;
        $config['maintain_ratio'] = TRUE;
        $config['remove_spaces'] = TRUE;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('logo')) {
            $this->session->set_flashdata('message', '<b>Image size Exceeds the limit of 200kb!</b>');
            redirect('cms/company');
        } else {
            $image = $this->upload->data();
            return $config['upload_path'] . $image['file_name'];
        }

    }
    /**! Image Upload **/

    /** CITY CRUD **/

    function city($param = '', $param2 = '')
    {
        $data = array();

        if ($param == 'create') {
            $data['city_Name'] = $this->input->post('city_Name');
            $data['status'] = $this->input->post('status');

            $this->db->insert('tbl_city', $data);

            $this->session->set_flashdata('message', '<b>City Added to Database!</b>');
            redirect(base_url() . 'cms/city', 'refresh');
        }
        if ($param == 'delete') {
            $this->db->where('city_Id', $param2);
            $this->db->delete('tbl_city');

            $this->session->set_flashdata('message', '<b>City information deleted!</b>');

            redirect(base_url() . 'cms/city', 'refresh');
        } elseif ($param == 'Edit') {
            $data['edit_data'] = $this->db->get_where('tbl_city', array(
                'city_Id' => $param2
            ))->result_array();
        } elseif ($param == 'do_update') {
            $data['city_Name'] = $this->input->post('city_Name');
            $data['status'] = $this->input->post('status');

            $this->db->where('city_Id', $param2);
            $this->db->update('tbl_city', $data);

            $this->session->set_flashdata('message', '<b>City information updated!</b>');

            redirect(base_url() . 'cms/city', 'refresh');
        }

        $data['page_name'] = 'City';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/city', $data, true);

        return $this->load->view('backend/index', $data);
    }

    /** AREA CRUD **/

    function area($param = '', $param2 = '')
    {
        $data = array();

        if ($param == 'create') {
            $data['area_Name'] = $this->input->post('area_Name');
            $data['city_Id'] = $this->input->post('city_Id');
            $data['status'] = $this->input->post('status');

            $this->db->insert('tbl_area', $data);

            $this->session->set_flashdata('message', '<b>Area Added to City Database!</b>');
            redirect(base_url() . 'cms/area', 'refresh');
        }
        if ($param == 'delete') {
            $this->db->where('area_Id', $param2);
            $this->db->delete('tbl_area');

            $this->session->set_flashdata('message', '<b>Area information deleted!</b>');

            redirect(base_url() . 'cms/area', 'refresh');
        } elseif ($param == 'Edit') {
            $data['edit_data'] = $this->db->get_where('tbl_area', array(
                'area_Id' => $param2
            ))->result_array();
        } elseif ($param == 'do_update') {
            $data['area_Name'] = $this->input->post('area_Name');
            $data['city_Id'] = $this->input->post('city_Id');
            $data['status'] = $this->input->post('status');

            $this->db->where('area_Id', $param2);
            $this->db->update('tbl_area', $data);

            $this->session->set_flashdata('message', '<b>Area information updated!</b>');

            redirect(base_url() . 'cms/area', 'refresh');
        }

        $data['page_name'] = 'Area';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/area', $data, true);

        return $this->load->view('backend/index', $data);
    }

    /** CATEGORY CRUD **/

    function category($param = '', $param2 = '')
    {
        $data = array();

        if ($param == 'create') {
            $data['category_Name'] = $this->input->post('category_Name');
            $data['status'] = $this->input->post('status');

            $this->db->insert('tbl_category', $data);

            $this->session->set_flashdata('message', '<b>Category Added to Database!</b>');
            redirect(base_url() . 'cms/category', 'refresh');
        }
        if ($param == 'delete') {
            $this->db->where('category_Id', $param2);
            $this->db->delete('tbl_category');

            $this->session->set_flashdata('message', '<b>Category information deleted!</b>');

            redirect(base_url() . 'cms/category', 'refresh');
        } elseif ($param == 'Edit') {
            $data['edit_data'] = $this->db->get_where('tbl_category', array(
                'category_Id' => $param2
            ))->result_array();
        } elseif ($param == 'do_update') {
            $data['category_Name'] = $this->input->post('category_Name');
            $data['status'] = $this->input->post('status');

            $this->db->where('category_Id', $param2);
            $this->db->update('tbl_category', $data);

            $this->session->set_flashdata('message', '<b>Category information updated!</b>');

            redirect(base_url() . 'cms/category', 'refresh');
        }

        $data['page_name'] = 'Category';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/category', $data, true);

        return $this->load->view('backend/index', $data);
    }

    /** SUBCATEGORY CRUD **/

    function subcategory($param = '', $param2 = '')
    {
        $data = array();

        if ($param == 'create') {
            $data['sub_Category_Name'] = $this->input->post('sub_Category_Name');
            $data['category_Id'] = $this->input->post('category_Id');
            $data['s_status'] = $this->input->post('s_status');

            $this->db->insert('tbl_subcategory', $data);

            $this->session->set_flashdata('message', '<b>Sub Category Added to Category Database!</b>');
            redirect(base_url() . 'cms/subcategory', 'refresh');
        }
        if ($param == 'delete') {
            $this->db->where('sub_Category_Id', $param2);
            $this->db->delete('tbl_subcategory');

            $this->session->set_flashdata('message', '<b>Sub Category information deleted!</b>');

            redirect(base_url() . 'cms/subcategory', 'refresh');
        } elseif ($param == 'Edit') {
            $data['edit_data'] = $this->db->get_where('tbl_subcategory', array(
                'sub_Category_Id' => $param2
            ))->result_array();
        } elseif ($param == 'do_update') {
            $data['sub_Category_Name'] = $this->input->post('sub_Category_Name');
            $data['category_Id'] = $this->input->post('category_Id');
            $data['s_status'] = $this->input->post('s_status');

            $this->db->where('sub_Category_Id', $param2);
            $this->db->update('tbl_subcategory', $data);

            $this->session->set_flashdata('message', '<b>Sub Category information updated!</b>');

            redirect(base_url() . 'cms/subcategory', 'refresh');
        }

        $data['page_name'] = 'Sub Category';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/subcategory', $data, true);

        return $this->load->view('backend/index', $data);
    }

    /** COMPANY CRUD **/


    function company($param = '', $param2 = '')
    {
        $data = array();

        if ($param == 'create') {
            $data['company_Name'] = $this->input->post('company_Name');
            $data['member_Status'] = $this->input->post('member_Status');
            $data['status'] = $this->input->post('status');

            $data['logo'] = $this->do_upload();

            $this->db->insert('tbl_company', $data);

            $this->session->set_flashdata('message', '<b>Company Info added Database!</b>');
            redirect(base_url() . 'cms/add_company', 'refresh');
        }
        if ($param == 'delete') {
            $this->db->where('company_Id', $param2);
            $this->db->delete('tbl_company');

            $this->session->set_flashdata('message', '<b>Company information deleted!</b>');

            redirect(base_url() . 'cms/manage_company', 'refresh');
        } elseif ($param == 'Edit') {
            $data['edit_data'] = $this->db->get_where('tbl_company', array(
                'company_Id' => $param2
            ))->result_array();
        } elseif ($param == 'do_update') {
            $data['company_Name'] = $this->input->post('company_Name');
            $data['member_Status'] = $this->input->post('member_Status');
            $data['status'] = $this->input->post('status');

            $data['logo'] = $this->do_upload();

            $this->db->where('company_Id', $param2);
            $this->db->update('tbl_company', $data);

            $this->session->set_flashdata('message', '<b>Company information updated!</b>');

            redirect(base_url() . 'cms/manage_company', 'refresh');
        }

        $data['page_name'] = 'Add Company';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/add_company', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function add_company()
    {
        $data = array();

        $data['page_name'] = 'Add Company';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/add_company', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function manage_company()
    {
        $data = array();

        $data['page_name'] = 'Manage Company';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/manage_company', $data, true);

        return $this->load->view('backend/index', $data);
    }

    /** COMPANY CATEGORY CRUD **/

    function company_category($param = '', $param2 = '')
    {
        $data = array();

        if ($param == 'create') {
            $sub_category_id = $this->input->post('sub_Category_Id[]');
            $total = count($sub_category_id);
            for ($i = 0; $i < $total; $i++) {

                $data['sub_Category_Id'] = $sub_category_id[$i];
                $data['company_Id'] = $this->input->post('company_Id');
                $data['category_Id'] = $this->input->post('category_Id');
                $this->db->insert('tbl_company_category', $data);
            }

            $this->session->set_flashdata('message', '<b>Company Category Added to Database!</b>');
            redirect(base_url() . 'cms/company_category', 'refresh');
        }
        if ($param == 'delete') {
            $this->db->where('id', $param2);
            $this->db->delete('tbl_company_category');

            $this->session->set_flashdata('message', '<b>Company Category information deleted!</b>');

            redirect(base_url() . 'cms/manage_company_category', 'refresh');
        } elseif ($param == 'Edit') {
            $data['edit_data'] = $this->db->get_where('tbl_company_category', array(
                'id' => $param2
            ))->result_array();
        } elseif ($param == 'do_update') {
            $data['company_Id'] = $this->input->post('company_Id');
            $data['category_Id'] = $this->input->post('category_Id');
            $data['sub_Category_Id'] = $this->input->post('sub_Category_Id');

            $this->db->where('id', $param2);
            $this->db->update('tbl_company_category', $data);

            $this->session->set_flashdata('message', '<b>Company Category information updated!</b>');

            redirect(base_url() . 'cms/manage_company_category', 'refresh');
        }

        $data['page_name'] = 'Add Company Category';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $this->load->model('crud_model');

        $this->db->select('*');
        $this->db->from('tbl_company c');
        $this->db->join('tbl_company_category cc', 'cc.company_Id = c.company_Id');
        $this->db->join('tbl_category ct', 'ct.category_Id = cc.category_Id');
        $this->db->join('tbl_subcategory s', 's.sub_Category_Id = cc.sub_Category_Id');
        $this->db->limit(10);
        $this->db->order_by('c.company_Id','desc');
        $query_result = $this->db->get()->result();

        $data['info'] = $query_result;

        $data['main_content'] = $this->load->view('backend/dynamic_files/company_category', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function manage_company_category()
    {
        $data['page_name'] = 'Manage Company Category';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $this->load->model('crud_model');

        $data['info'] = $this->crud_model->get_company_category_info();

        $data['main_content'] = $this->load->view('backend/dynamic_files/manage_company_category', $data, true);

        return $this->load->view('backend/index', $data);
    }

    //FETCHING SUB CATEGORY ID BY CATEGORY ID

    function get_subcat_id($cat_id)
    {
        $id = $cat_id;

        $result = $this->db->get_where("tbl_subcategory", array('category_Id' => $id))->result();
        echo json_encode($result);

    }

    /**BRANCH CRUD **/
    function branch($param = '', $param2 = '')
    {
        $data = array();

        if ($param == 'create') {

            $data['company_Id'] = $this->input->post('company_Id');
            $data['area_Id'] = $this->input->post('area_Id');
            $data['branch_Name'] = $this->input->post('branch_Name');
            $data['branch_Name'] = $this->input->post('branch_Name');
            $data['service_type'] = $this->input->post('service_type');
            $data['address'] = $this->input->post('address');
            $data['phone'] = $this->input->post('phone');
            $data['web'] = $this->input->post('web');
            $data['meta_Name'] = $this->input->post('meta_Name');
            $data['meta_Keyword'] = $this->input->post('meta_Keyword');
            $data['email'] = $this->input->post('email');
            $data['verification_status'] = $this->input->post('verification_status');

            $this->db->insert('tbl_branch', $data);

            $this->session->set_flashdata('message', '<b>Branch Information Added to Database!</b>');
            redirect(base_url() . 'cms/branch', 'refresh');
        }
        if ($param == 'delete') {
            $this->db->where('branch_Id', $param2);
            $this->db->delete('tbl_branch');

            $this->session->set_flashdata('message', '<b>Branch Information deleted!</b>');

            redirect(base_url() . 'cms/manage_branch', 'refresh');
        } elseif ($param == 'Edit') {
            $data['edit_data'] = $this->db->get_where('tbl_company_category', array(
                'id' => $param2
            ))->result_array();
        } elseif ($param == 'do_update') {
            $data['company_Id'] = $this->input->post('company_Id');
            $data['area_Id'] = $this->input->post('area_Id');
            $data['branch_Name'] = $this->input->post('branch_Name');
            $data['branch_Name'] = $this->input->post('branch_Name');
            $data['service_type'] = $this->input->post('service_type');
            $data['address'] = $this->input->post('address');
            $data['phone'] = $this->input->post('phone');
            $data['web'] = $this->input->post('web');
            $data['meta_Name'] = $this->input->post('meta_Name');
            $data['meta_Keyword'] = $this->input->post('meta_Keyword');
            $data['email'] = $this->input->post('email');
            $data['verification_status'] = $this->input->post('verification_status');

            $this->db->where('branch_Id', $param2);
            $this->db->update('tbl_branch', $data);

            $this->session->set_flashdata('message', '<b>Branch Information updated!</b>');

            redirect(base_url() . 'cms/manage_branch', 'refresh');
        }

        $data['page_name'] = 'Add Branch';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $this->load->model('crud_model');

        $data['main_content'] = $this->load->view('backend/dynamic_files/branch', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function manage_branch()
    {
        $data['page_name'] = 'Manage Branch';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $this->load->model('crud_model');

        $data['main_content'] = $this->load->view('backend/dynamic_files/manage_branch', $data, true);

        return $this->load->view('backend/index', $data);
    }


    //FETCHING AREA ID BY CITY ID

    function get_area_id($city_id)
    {
        $id = $city_id;

        $result = $this->db->get_where("tbl_area", array('city_Id' => $id))->result();
        echo json_encode($result);

    }

    function about_us($param='')
    {
        if ($param == 'create') {
            $data['description'] = $this->input->post('about_us');

            $this->db->where('info_type', 'about_us');
            $this->db->update('frontend_descriptions', $data);

            $this->session->set_flashdata('message', '<b>About Us Info Saved!</b>');
            redirect(base_url() . 'cms/about_us', 'refresh');
        }

        $data['page_name'] = 'About Us';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/about_us', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function advertise_info($param='')
    {
        if ($param == 'create') {
            $data['description'] = $this->input->post('advertise_info');

            $this->db->where('info_type', 'advertise_info');
            $this->db->update('frontend_descriptions', $data);

            $this->session->set_flashdata('message', '<b>Advertise Info Saved!</b>');
            redirect(base_url() . 'cms/advertise_info', 'refresh');
        }

        $data['page_name'] = 'Advertise Info';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/advertise_info', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function advertise_with_us($param='')
    {
        if ($param == 'create') {
            $data['description'] = $this->input->post('advertise_with_us');

            $this->db->where('info_type', 'advertise_with_us');
            $this->db->update('frontend_descriptions', $data);

            $this->session->set_flashdata('message', '<b>Advertise with Us Info Saved!</b>');
            redirect(base_url() . 'cms/advertise_with_us', 'refresh');
        }

        $data['page_name'] = 'Advertise with us';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/advertise_with_us', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function contact($param='')
    {
        if ($param == 'create') {
            $data['description'] = $this->input->post('contact');

            $this->db->where('info_type', 'contact');
            $this->db->update('frontend_descriptions', $data);

            $this->session->set_flashdata('message', '<b>About Us Info Saved!</b>');
            redirect(base_url() . 'cms/contact', 'refresh');
        }

        $data['page_name'] = 'Contact';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/contact', $data, true);

        return $this->load->view('backend/index', $data);
    }

    function feedback($param='')
    {
        if ($param == 'create')
        {
            $data['description'] = $this->input->post('feedback');

            $this->db->where('info_type', 'feedback');
            $this->db->update('frontend_descriptions', $data);

            $this->session->set_flashdata('message', '<b>About Us Info Saved!</b>');
            redirect(base_url() . 'cms/feedback', 'refresh');
        }

        $data['page_name'] = 'Feedback';

        $data['top'] = $this->load->view('backend/include_top', $data, true);
        $data['bottom'] = $this->load->view('backend/include_bottom', $data, true);
        $data['navigation'] = $this->load->view('backend/navigation', $data, true);

        $data['main_content'] = $this->load->view('backend/dynamic_files/feedback', $data, true);

        return $this->load->view('backend/index', $data);
    }


}