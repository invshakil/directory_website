<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{


    public function index()
    {
        $data = array();
        $data['header'] = $this->load->view('frontend/includes/header', '', true);
        $data['footer'] = $this->load->view('frontend/includes/footer', '', true);
        $data['top_menu'] = $this->load->view('frontend/includes/top_menu', '', true);
        $data['top_adspace'] = $this->load->view('frontend/includes/top_adspace', '', true);
        $data['search_area'] = $this->load->view('frontend/includes/search_area', '', true);
        $data['ad_space_top_2'] = $this->load->view('frontend/includes/ad_space_top_2', '', true);
        $data['left_side_bar'] = $this->load->view('frontend/includes/left_side_bar', '', true);
        $data['right_side_bar'] = $this->load->view('frontend/includes/right_side_bar', '', true);

        $data['main_content'] = $this->load->view('frontend/index_content', $data, true);
        $this->load->view('frontend/index', $data);
    }

    public function search()
    {

        $search = $this->input->post('search_data');
        $query = $this->crud_model->searchResult($search);
        foreach ($query as $row):
            echo "<li class='list-group-item'><input type='hidden' name='company' value='$row->company_Name'>" . $row->company_Name . "</li>";
        endforeach;

    }

    public function search_result()
    {
        $data = array();
        $city = $this->input->get('city');
        $area = $this->input->get('area');
        $company = $this->input->get('company1');

        $name = $this->db->get_where('tbl_area', array('area_Id' => $area))->row('area_Name');

        if ($city == 0 && $company != '') {
//            $result = $this->crud_model->search_result($company);
            $data['companies_info'] = $this->crud_model->search_result($company);
         }

        else{
            $data['companies_info'] = $this->crud_model->search_result_area($company,$city,$area);

        }

        $data['count_result'] = count($data['companies_info']);
        $data['search_keyword'] = $company;

        $data['header'] = $this->load->view('frontend/includes/header', '', true);
        $data['footer'] = $this->load->view('frontend/includes/footer', '', true);
        $data['top_menu'] = $this->load->view('frontend/includes/top_menu', '', true);
        $data['top_adspace'] = $this->load->view('frontend/includes/top_adspace', '', true);
        $data['search_area'] = $this->load->view('frontend/includes/search_area', '', true);
        $data['ad_space_top_2'] = $this->load->view('frontend/includes/ad_space_top_2', '', true);
        $data['left_side_bar'] = FALSE;
        $data['right_side_bar'] = False;

        $data['main_content'] = $this->load->view('frontend/search_result', $data, true);
        $this->load->view('frontend/index', $data);
    }

    public function company_details_info($id)
    {
        $data['header'] = $this->load->view('frontend/includes/header', '', true);
        $data['footer'] = $this->load->view('frontend/includes/footer', '', true);
        $data['top_menu'] = $this->load->view('frontend/includes/top_menu', '', true);
        $data['top_adspace'] = $this->load->view('frontend/includes/top_adspace', '', true);
        $data['search_area'] = $this->load->view('frontend/includes/search_area', '', true);
        $data['ad_space_top_2'] = $this->load->view('frontend/includes/ad_space_top_2', '', true);
        $data['left_side_bar'] = FALSE;
        $data['right_side_bar'] = False;
        $data['row'] = $this->crud_model->get_company_details($id);
        $data['main_content'] = $this->load->view('frontend/company_info_by_id', $data, true);
        $this->load->view('frontend/index', $data);
    }

    public function category_info($id)
    {

        $data = array();
        $data['header'] = $this->load->view('frontend/includes/header', '', true);
        $data['footer'] = $this->load->view('frontend/includes/footer', '', true);
        $data['top_menu'] = $this->load->view('frontend/includes/top_menu', '', true);
        $data['top_adspace'] = $this->load->view('frontend/includes/top_adspace', '', true);
        $data['search_area'] = $this->load->view('frontend/includes/search_area', '', true);
        $data['ad_space_top_2'] = FALSE;
        $data['left_side_bar'] = FALSE;
        $data['right_side_bar'] = FALSE;

        $data['sub_category_info'] = $this->crud_model->get_sub_category_info($id);
        $info = $this->db->get_where('tbl_category', array('category_Id' => $id))->row();
        $data['category_name'] = $info->category_Name;

        $data['main_content'] = $this->load->view('frontend/category_content', $data, true);
        $this->load->view('frontend/index', $data);
    }

    public function companies($id, $url = 1)
    {

        $data = array();
        $data['header'] = $this->load->view('frontend/includes/header', '', true);
        $data['footer'] = $this->load->view('frontend/includes/footer', '', true);
        $data['top_menu'] = $this->load->view('frontend/includes/top_menu', '', true);
        $data['top_adspace'] = $this->load->view('frontend/includes/top_adspace', '', true);
        $data['search_area'] = $this->load->view('frontend/includes/search_area', '', true);
        $data['ad_space_top_2'] = $this->load->view('frontend/includes/ad_space_top_2', '', true);
        $data['left_side_bar'] = FALSE;
        $data['right_side_bar'] = FALSE;

        $row = $this->db->get_where('tbl_company_category', array('sub_Category_Id'=>$id))->result();

        $count = count($row); //returns total row

        $data['total_result'] = $count;
        $data['per_page'] = 10;

        /* Pagination Config */
        $config = array();
        $config["base_url"] = base_url() . "/welcome/companies/".$id.'/'.$url.'/';
        $config["total_rows"] = $count;
        $config["per_page"] = $data['per_page'];
        $config["num_links"] = false;

        /* BootStrap pagination Button Settings */
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        /* !-BootStrap pagination Button Settings */

        /* !- Pagination Config */
        $this->pagination->initialize($config);

        $str_links = $this->pagination->create_links();
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $data["links"] = explode('&nbsp;', $str_links);

        $data['companies_info'] = $this->crud_model->get_companies_info($id, $config["per_page"], $page);

        if ($this->uri->segment(5) == NULL)
        {
            $record = 1;
        }else
        {
            $record = floor( ($this->uri->segment(5)/10)+1);
        }

        $data['start_page'] = $record;

        $float_check = $count/10;

        if (is_float($float_check))
        {
            $total_page = explode('.',$float_check);
            $page = $total_page[0]+1;
        }
        else{
            $page = $count/10;
        }



        $data['total_page'] = $page;


        $info = $this->crud_model->get_category_info($id);
        $data['category_name'] = $info->category_Name;
        $data['category_id'] = $info->category_Id;

        $data['sub_category_name'] = $this->db->get_where('tbl_subcategory', array('sub_Category_Id' => $id))->row();

        $data['main_content'] = $this->load->view('frontend/companies_list', $data, true);
        $this->load->view('frontend/index', $data);
    }

    public function get_area_id($city)
    {
        $id = $city;

        $result = $this->db->order_by('area_Name', 'ASC')->get_where("tbl_area", array('city_Id' => $id, 'status' => 1))->result();
        echo json_encode($result);
    }

    public function search_details()
    {
        $data = array();
        $data['header'] = $this->load->view('frontend/includes/header', '', true);
        $data['footer'] = $this->load->view('frontend/includes/footer', '', true);
        $data['top_menu'] = $this->load->view('frontend/includes/top_menu', '', true);
        $data['top_adspace'] = $this->load->view('frontend/includes/top_adspace', '', true);
        $data['search_area'] = $this->load->view('frontend/includes/search_area', '', true);
        $data['ad_space_top_2'] = $this->load->view('frontend/includes/ad_space_top_2', '', true);
        $data['left_side_bar'] = FALSE;
        $data['right_side_bar'] = $this->load->view('frontend/includes/right_side_bar', '', true);

        $data['main_content'] = $this->load->view('frontend/search_content', $data, true);
        $this->load->view('frontend/index', $data);
    }

    public function about_us()
    {
        $data = array();
        $data['header'] = $this->load->view('frontend/includes/header', '', true);
        $data['footer'] = $this->load->view('frontend/includes/footer', '', true);
        $data['top_menu'] = $this->load->view('frontend/includes/top_menu', '', true);
        $data['top_adspace'] = $this->load->view('frontend/includes/top_adspace', '', true);
        $data['search_area'] = $this->load->view('frontend/includes/search_area', '', true);
        $data['ad_space_top_2'] = $this->load->view('frontend/includes/ad_space_top_2', '', true);
        $data['left_side_bar'] = FALSE;
        $data['right_side_bar'] = $this->load->view('frontend/includes/right_side_bar', '', true);

        $data['main_content'] = $this->load->view('frontend/about_us', $data, true);
        $this->load->view('frontend/index', $data);
    }

    public function advertise_info()
    {
        $data = array();
        $data['header'] = $this->load->view('frontend/includes/header', '', true);
        $data['footer'] = $this->load->view('frontend/includes/footer', '', true);
        $data['top_menu'] = $this->load->view('frontend/includes/top_menu', '', true);
        $data['top_adspace'] = $this->load->view('frontend/includes/top_adspace', '', true);
        $data['search_area'] = $this->load->view('frontend/includes/search_area', '', true);
        $data['ad_space_top_2'] = $this->load->view('frontend/includes/ad_space_top_2', '', true);
        $data['left_side_bar'] = FALSE;
        $data['right_side_bar'] = $this->load->view('frontend/includes/right_side_bar', '', true);

        $data['main_content'] = $this->load->view('frontend/advertise_info', $data, true);
        $this->load->view('frontend/index', $data);
    }

    public function advertise_with_us()
    {
        $data = array();
        $data['header'] = $this->load->view('frontend/includes/header', '', true);
        $data['footer'] = $this->load->view('frontend/includes/footer', '', true);
        $data['top_menu'] = $this->load->view('frontend/includes/top_menu', '', true);
        $data['top_adspace'] = $this->load->view('frontend/includes/top_adspace', '', true);
        $data['search_area'] = $this->load->view('frontend/includes/search_area', '', true);
        $data['ad_space_top_2'] = $this->load->view('frontend/includes/ad_space_top_2', '', true);
        $data['left_side_bar'] = FALSE;
        $data['right_side_bar'] = $this->load->view('frontend/includes/right_side_bar', '', true);

        $data['main_content'] = $this->load->view('frontend/advertise_with_us', $data, true);
        $this->load->view('frontend/index', $data);
    }

    public function contact()
    {
        $data = array();
        $data['header'] = $this->load->view('frontend/includes/header', '', true);
        $data['footer'] = $this->load->view('frontend/includes/footer', '', true);
        $data['top_menu'] = $this->load->view('frontend/includes/top_menu', '', true);
        $data['top_adspace'] = $this->load->view('frontend/includes/top_adspace', '', true);
        $data['search_area'] = $this->load->view('frontend/includes/search_area', '', true);
        $data['ad_space_top_2'] = $this->load->view('frontend/includes/ad_space_top_2', '', true);
        $data['left_side_bar'] = FALSE;
        $data['right_side_bar'] = $this->load->view('frontend/includes/right_side_bar', '', true);

        $data['main_content'] = $this->load->view('frontend/contact', $data, true);
        $this->load->view('frontend/index', $data);
    }


    public function feedback()
    {
        $data = array();
        $data['header'] = $this->load->view('frontend/includes/header', '', true);
        $data['footer'] = $this->load->view('frontend/includes/footer', '', true);
        $data['top_menu'] = $this->load->view('frontend/includes/top_menu', '', true);
        $data['top_adspace'] = $this->load->view('frontend/includes/top_adspace', '', true);
        $data['search_area'] = $this->load->view('frontend/includes/search_area', '', true);
        $data['ad_space_top_2'] = $this->load->view('frontend/includes/ad_space_top_2', '', true);
        $data['left_side_bar'] = FALSE;
        $data['right_side_bar'] = $this->load->view('frontend/includes/right_side_bar', '', true);

        $data['main_content'] = $this->load->view('frontend/feedback', $data, true);
        $this->load->view('frontend/index', $data);
    }

}
