<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Crud_model extends CI_Model
{
    public function searchResult($search)
    {
        $this->db->select("*");
        $this->db->from('tbl_company');
        $this->db->group_start();
        $this->db->like('company_Name', $search);
        $this->db->group_end();
        $this->db->limit(15);
        $this->db->order_by('company_Id', 'asc');
        $query = $this->db->get();
        return $query->result();

    }

    public function search_result($company)
    {
        $this->db->select('*');
        $this->db->from('tbl_company t');
        $this->db->join('tbl_branch b','b.company_Id = t.company_Id');
        $this->db->join('tbl_area a','a.area_Id = b.area_Id');
        $this->db->join('tbl_city c','c.city_Id = a.city_Id');
        $this->db->group_start();
        $this->db->like('t.company_Name', $company);
        $this->db->group_end();
        $this->db->limit(100);
        $query = $this->db->get();
        return $query->result();

    }


    public function search_result_area($company,$city,$area)
    {
        $this->db->select('*');
        $this->db->from('tbl_company t');
        $this->db->join('tbl_branch b','b.company_Id = t.company_Id');
        $this->db->join('tbl_area a','a.area_Id = b.area_Id');
        $this->db->join('tbl_city c','c.city_Id = a.city_Id');
        $this->db->group_start();
        $this->db->like('t.company_Name', $company);
        $this->db->group_end();
        $this->db->limit(100);
        $this->db->where('a.area_Id',$area);
        $this->db->where('a.city_Id',$city);
        $query = $this->db->get();
        return $query->result();
    }
    public function search_result_city_area($company,$city,$area)
    {
        $this->db->select('*');
        $this->db->from('tbl_company t');
        $this->db->join('tbl_branch b','b.company_Id = t.company_Id');
        $this->db->join('tbl_area a','a.area_Id = b.area_Id');
        $this->db->join('tbl_city c','c.city_Id = a.city_Id');
        $this->db->group_start();
        $this->db->like('t.company_Name', $company);
        $this->db->group_end();
        $this->db->limit(100);
        $this->db->where('c.city_Id',$city);
        $this->db->where('a.area_Name',$area);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_company_details($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_company t');
        $this->db->join('tbl_branch b','b.company_Id = t.company_Id');
        $this->db->join('tbl_area a','a.area_Id = b.area_Id');
        $this->db->join('tbl_city c','c.city_Id = a.city_Id');
        $this->db->where('b.branch_Id',$id);
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->row();
            return $result;
        }
        return false;
    }
    public function get_sub_category_info($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_subcategory');
        $where = "s_status = '1' AND category_Id ='$id'";
        $this->db->where($where);
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();
            return $result;
        }
        return false;

    }

    public function get_category_info($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_subcategory s');
        $this->db->join('tbl_category c', 'c.category_Id = s.category_Id');
        $where = "sub_Category_Id ='$id'";
        $this->db->where($where);
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->row();
            return $result;
        }
        return false;

    }

    public function get_companies_info($id, $limit, $start)
    {

        $this->db->select('*');

        $this->db->from('tbl_company_category c');
        $this->db->join('tbl_company m','m.company_Id = c.company_Id');
        $this->db->join('tbl_branch t','t.company_Id = c.company_Id');
        $this->db->join('tbl_subcategory s','s.sub_Category_Id = c.sub_Category_Id');
        $this->db->where('c.sub_Category_Id',$id);
//        $this->db->order_by('c.company_Id','ASC');
        $this->db->order_by('m.company_Name','ASC');
        $this->db->limit($limit,$start);
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();
            return $result;
        }
        return false;
    }
	
	function get_company_category_info()
    {
        $this->db->select('*');
        $this->db->from('tbl_company c');
        $this->db->join('tbl_company_category cc', 'cc.company_Id = c.company_Id');
        $this->db->join('tbl_category ct', 'ct.category_Id = cc.category_Id');
        $this->db->join('tbl_subcategory s', 's.sub_Category_Id = cc.sub_Category_Id');
        $query_result = $this->db->get();

        if ($query_result->num_rows() > 0) {
            $result = $query_result->result();
//            echo '<pre>';print_r($result);exit;
            return $result;
        }
        return false;
    }
}