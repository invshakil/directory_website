<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>
    <!--Header File-->
    <?php echo $header; ?>
</head>

<body class="home page-template page-template-frontpage page-template-frontpage-php page page-id-4">
<section>
    <!--Top Menu-->
    <?php echo $top_menu; ?>
</section>
<section>
    <!--top Adspace-->
    <?php echo $top_adspace; ?>
</section>


<section>
    <!--Search Area-->
    <?php echo $search_area; ?>
</section>

<section>
    <!--2nd AdSpace-->
    <?php echo $ad_space_top_2; ?>
</section>


<div class="section wc">

    <div class="container">

        <!-- Row One-->

        <div class="row padding5">

            <!--Left Side Bar-->
            <?php echo $left_side_bar; ?>

            <!--Dynamic Page Content-->
            <?php echo $main_content; ?>

            <!--Right Side Bar-->
            <?php echo $right_side_bar; ?>

        </div>
    </div>

</div>

<!--Footer Content With JS FILE-->
<?php echo $footer; ?>
</body>
</html>