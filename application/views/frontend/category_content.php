<div class="col-md-10" style="padding-left:10px!important;">

    <div class="row" style="padding-top: 10px; padding-bottom: 10px;">
        <div class="col-md-4">
            <div class="right-content common-content four">
                <figure style="height: 150px;background-color: #2C3E50; color: #1abc9c;">
                    <figcaption>
                        <div class="img-content">
                            <h2><?php echo $category_name;?></h2>
                        </div>
                    </figcaption>
                </figure>
            </div>
        </div>
        <div class="col-md-8">
            <div class="right-content common-content">
                <img class="img-responsive" src="http://via.placeholder.com/750x175">
            </div>

        </div>
    </div>


    <div class="row">

        <?php
        if (is_array($sub_category_info) && count($sub_category_info) >= 1) {

        foreach ($sub_category_info as $row) {
        ?>

        <div class="col-md-3">
            <div class="left-content common-content four">
                <figure class="image" style="height: 120px; font-weight: bolder;">
                    <figcaption>
                        <div class="img-content">
                            <p class="subcat_header" style="color: black!important;"><a
                                    href="<?php echo base_url();?>companies/<?php echo $row->sub_Category_Id;?>/1">
                                    <?php echo $row->sub_Category_Name;?>
                                </a></p>
                        </div>
                    </figcaption>
                </figure>
            </div>

        </div>
        <?php }}?>


    </div>

</div>
<div class="col-md-2">
    <aside id="secondary" class="sidebar widget-area" role="complementary">
        <div id="text-5" class="sidebar widget_text">
            <div class="textwidget">
                <img src="http://placehold.it/180x200/bdc3c7/000000?text=Ad+Space">
                <img src="http://placehold.it/180x200/bdc3c7/000000?text=Ad+Space">
                <img src="http://placehold.it/180x200/bdc3c7/000000?text=Ad+Space">
                <img src="http://placehold.it/180x200/bdc3c7/000000?text=Ad+Space">
                <img src="http://placehold.it/180x200/bdc3c7/000000?text=Ad+Space">
            </div>
        </div>
    </aside>
</div>


<script>
    $(".image").each(function() {
        var hue = 'rgb(' + (Math.floor((75-25)*Math.random()) + 75) + ',' + (Math.floor((75-25)*Math.random()) + 75) + ',' + (Math.floor((75-25)*Math.random()) + 75) + ')';
        $(this).css("background-color", hue);
    });
</script>