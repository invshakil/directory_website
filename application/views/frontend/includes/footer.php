<footer style="margin-top: 5px;">
    <div class="container">
        <div class="footer-back">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-content text-center">

                        <div class="menu-footer-menu-container">
                            <ul id="menu-footer-menu" class="menu">
                                <li id="menu-item-15"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15"><a
                                        href="<?php echo base_url() ?>about-us/">About Us</a></li>
                                <li id="menu-item-17"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-17"><a
                                        href="<?php echo base_url() ?>advertising-information/">Advertising
                                        Info</a></li>
                                <li id="menu-item-18"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-17"><a
                                        href="<?php echo base_url() ?>advertise-request/">Advertise
                                        With Us</a></li>
                                <li id="menu-item-19"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-19"><a
                                        href="<?php echo base_url() ?>contact-us/">Contact us</a></li>
                                <li id="menu-item-8025"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8025"><a
                                        href="<?php echo base_url() ?>feedback/">Feedback</a>
                                </li>
                            </ul>
                        </div>

                        <h5>Copyright © boichitrabangladeshdirectory.com All rights reserved By Boichitra Media
                            Ltd.</h5>
                        <h6>Reproduction in whole or in part without permission is prohibited</h6>
                    </div>

                    <div class="footer-social text-center">
                        <ul>
                            <li><a href="#" class="hvr-wobble-vertical"><img
                                        src="<?php echo base_url() ?>wp-content/themes/boichitra1/boichitra/images/icon/facebook.png"></a>
                            </li>
                            <li><a href="#" class="hvr-wobble-vertical"><img
                                        src="<?php echo base_url() ?>wp-content/themes/boichitra1/boichitra/images/icon/google-plus.png"></a>
                            </li>
                            <li><a href="#" class="hvr-wobble-vertical"><img
                                        src="<?php echo base_url() ?>wp-content/themes/boichitra1/boichitra/images/icon/instagram.png"></a>
                            </li>
                            <li><a href="#" class="hvr-wobble-vertical"><img
                                        src="<?php echo base_url() ?>wp-content/themes/boichitra1/boichitra/images/icon/linkedin.png"></a>
                            </li>
                            <li><a href="#" class="hvr-wobble-vertical"><img
                                        src="https://www.mnsu.edu/images/icons/twitter-icon.png"></a></li>

                        </ul>
                    </div>

                </div>
            </div>

        </div>
    </div>
</footer>
<a href="#" class="back-to-top" style="display: inline;">

    <i class="fa fa-arrow-circle-up fa-3x" style="padding-left: 10px;padding-top: 10px"></i>

</a>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" >
<!--<script type='text/javascript'-->
<!--        src='<?php echo base_url() ?>wp-content/themes/boichitra1/boichitra/js/skip-link-focus-fix.js?ver=20160816'></script>-->
<script type='text/javascript'
        src='<?php echo base_url() ?>assets/js/bootstrap.min.js'></script>
<!--<script type='text/javascript'-->
<!--        src='<?php echo base_url() ?>wp-content/themes/boichitra1/boichitra/js/main.js?ver=20160816'></script>-->
<script type='text/javascript'>
    /* <![CDATA[ */
    var screenReaderText = {"expand": "expand child menu", "collapse": "collapse child menu"};
    /* ]]> */
</script>
<script type='text/javascript'
        src='<?php echo base_url() ?>assets/js/function.js'></script>

<script>

    jQuery(document).ready(function () {

        var offset = 250;

        var duration = 300;

        jQuery(window).scroll(function () {

            if (jQuery(this).scrollTop() > offset) {

                jQuery('.back-to-top').
                fadeIn(duration);

            } else {

                jQuery('.back-to-top').
                fadeOut(duration);

            }

        });


        jQuery('.back-to-top').
        click(function (event) {

            event.preventDefault();

            jQuery('html, body').
            animate({scrollTop: 0}, duration);

            return false;

        })

    });

</script>

<!--<script src="//code.jquery.com/jquery-1.11.2.min.js"></script-->
