<div class="container">


    <div class="header-back" style="padding-top: 5px!important;">
        <form role="search" method="get" class="search-form" action="<?php echo base_url(); ?>search_result">

            <div class="row">
                <div class="col-md-12">
                    <div class="head">
                        <h1>Boichitra Bangladesh Directory</h1>

                        <div class="input-group col-md-12">
                            <div class="col-md-3"></div>
                            <div class="col-md-5">
                                <input type="search" class="form-control" aria-describedby="basic-addon2"
                                       name="company1" id="search_data" onkeyup="lookup(this.value);" onblur="outoffocus()" onfocus="lookup(this.value);"
                                       placeholder="What are you looking for" required/>

                                <div id="suggestions">
                                    <ul class="list-group" id="autoSuggestionsList">
                                    </ul>
                                </div>

                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-success"><b>SEARCH</b></button>
                            </div>


                        </div>


                    </div>
                </div>

            </div>
            <div class="row" style="margin-top: 20px">
                <div class="col-md-12">
                    <div class="col-md-6 pull-left">
                        <div class="head-dropdown">
                            <ul>
                                <li>
                                    <select name="city">
                                        <option value="0" selected>Select City</option>
                                        <?php
                                        $info = $this->db->order_by('city_Name','ASC')->get_where('tbl_city', array('status' => 1))->result_array();
                                        foreach ($info as $row) {
                                            ?>
                                            <option value="<?php echo $row['city_Id']; ?>">
                                                <?php echo $row['city_Name']; ?>
                                            </option>

                                        <?php } ?>

                                    </select>
                                </li>
                                <li>
                                    <select name="area" id="area_id">
                                        <option value="0">Select Area</option>

                                    </select>
                                </li>
                            </ul>

                        </div>
                    </div>
                    <div class="col-md-4 pull-right">
                        <div class="membership text-right">
                            <ul>
                                <li>
                                    <a href="http://www.boichitrabangladeshdirectory.com/free-listing/">Free Listing</a>
                                </li>
                                <li>
                                    <a href="http://www.boichitrabangladeshdirectory.com/membership-request/">Membership</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </form>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('select[name="city"]').on('change', function () {
            var city = $(this).val();
            if (city != '') {
                $.ajax({
                    url: '<?php echo base_url()?>welcome/get_area_id/' + city,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {

                        $('select[name="area"]').empty();
                        $.each(data, function (key, value) {
                            $('select[name="area"]').append('<option value="'+ value.area_Id +'">' + value.area_Name + '</option>');
                        });
                    }
                });
            }

            else {
                $('select[name="area"]').empty();
            }
        });
    });
</script>

<script>


    function lookup(search_data) {
        var input_data = $('#search_data').val();
        if (input_data.length === 0) {
            $('#suggestions').hide();
        } else {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>welcome/search",
                data: {search_data: input_data},
                success: function (data) {
                    // return success
                    if (data.length > 0) {
                        $('#suggestions').show();
                        $('#autoSuggestionsList').html(data);
                        $("#autoSuggestionsList li").on("click", function() {
                            fill($(this).text());
                        });
                    }
                }
            });
        }
    }

    function fill(thisValue) {
        $('#search_data').val(thisValue);
        setTimeout(function() {
            $('#suggestions').hide(); }, 200);
    }
    function outoffocus() {
        setTimeout(function() {
            $('#suggestions').hide();}, 200);
    }
</script>