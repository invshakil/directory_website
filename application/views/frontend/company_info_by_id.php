<?php //print_r($category_info);exit();?>
<div class="col-sm-10">

            <div class="row">

                <div class="col-sm-3">
                    <div class="data-left">
                        <?php if ($row->logo == '') {?>
                            <img class="img-responsive" src="<?php echo base_url() ?>assets/upload/company_logo.jpg"/>
                        <?php } else {?>
                        <img class="img-responsive" src="<?php echo base_url().$row->logo; ?>" height="200" width="200"/>
                        <?php } ?>
                        <ul>
                            <li><a href="<?php echo $row->web;?>" target="_blank" class="btn btn-info">Web</a></li>
                            <li><a href="mailto:<?php echo $row->email;?>" class="btn btn-info">Email</a></li>
                        </ul>

                    </div>
                </div>
                <div class="col-sm-9 container-fluid">


                    <table id="company_info" class="table table-striped">
                        <thead class="thead-inverse">
                        <tr>
                            <th class="info" width="30%"
                                style="background-color: #E5E5D8; color: black;border-radius:5px;">
                                Company/Person Name
                            </th>
                            <th style="background-color: #F2E6AA; color: #592C16;border-radius:5px; height: 1px !important;">
                                <a href="<?php echo base_url()?>welcome/company_details_info/<?php echo $row->branch_Id;?>">
                                    <b style="color: #0a001f"><?php echo $row->company_Name; ?></b>

                                    <?php
                                    if ($row->verification_status == 1)
                                    {
                                        echo '<img src="'.base_url().'download.png" width="30">';
                                    }
                                    ?>
                                </a>

                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr style="height: 5px;">
                            <td style="color: black;background-color: #E5E5D8 ;border-radius:5px;">
                                Business/Service Type
                            </td>
                            <td style="border-radius:5px; padding: 10px;;"><?php echo $row->service_type;?></td>
                        </tr>
                        <tr>
                            <td style="color: black;background-color: #E5E5D8 ;border-radius:5px;">
                                Address
                            </td>
                            <td style="border-radius:5px; padding: 10px;;"><?php echo $row->address; ?></td>
                        </tr>
                        <tr>
                            <td style="color: black;background-color: #E5E5D8 ;border-radius:5px;">
                                City
                            </td>
                            <td style="border-radius:5px; padding: 10px;;"><?php echo $row->city_Name; ?></td>
                        </tr>
                        <tr>
                            <td style="color: black;background-color: #E5E5D8 ;border-radius:5px;">
                                Area/Thana
                            </td>
                            <td style="border-radius:5px; padding: 10px;;"><?php echo $row->area_Name; ?></td>
                        </tr>
                        <tr>
                            <td style="color: black;background-color: #E5E5D8 ;border-radius:5px;">
                                Phone
                            </td>
                            <td style="border-radius:5px; padding: 10px;;"><?php echo $row->phone; ?></td>
                        </tr>
                        <tr>
                            <td style="color: black;background-color: #E5E5D8 ;border-radius:5px;">
                                Email
                            </td>
                            <td style="border-radius:5px; padding: 10px;;"><?php echo $row->email; ?></td>
                        </tr>
                        <tr>
                            <td style="color: black;background-color: #E5E5D8 ;border-radius:5px;">
                                Web
                            </td>
                            <td style="border-radius:5px; padding: 10px;;"><?php echo $row->web; ?></td>
                        </tr>

                        </tbody>
                    </table>


                </div>

            </div>

</div>
<div class="col-md-2">
    <aside id="secondary" class="sidebar widget-area" role="complementary">
        <div id="text-5" class="sidebar widget_text">
            <div class="textwidget">
                <img src="http://placehold.it/180x200/bdc3c7/000000?text=Ad+Space">
                <img src="http://placehold.it/180x200/bdc3c7/000000?text=Ad+Space">
            </div>
        </div>
    </aside>
</div>