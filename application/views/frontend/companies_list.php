<?php //print_r($category_info);exit();?>
<div class="col-sm-10">
    <div class="row">

        <div class="col-md-12">

            <div class="">


                <ul id="breadcrumbs" class="breadcrumbs">
                    <li class="item-home"><a class="" href="<?php echo base_url() ?>" title="Home">Home</a>
                    </li>
                    <li class="item-24"><a href="<?php echo base_url(); ?>category_info/<?php echo $category_id; ?>">
                            <?php echo $category_name ?></a>
                    </li>
                    <li class="item-24"><a href=""><?php echo $sub_category_name->sub_Category_Name; ?></a></li>
                </ul>
            </div>

        </div>

    </div>
    <div class="row">
        <!--        <div class="pull-right">-->
        <!--            <p class="label label-info">Showing page 1 of 30, total records 300. Showing record 20.</p>-->
        <!--            <nav aria-label="Page navigation example">-->
        <!--                <ul class="pagination">-->
        <!--                    <li class="page-item"><a class="page-link" href="#">Previous</a></li>-->
        <!--                    <li class="page-item"><a class="page-link" href="#">1</a></li>-->
        <!--                    <li class="page-item"><a class="page-link" href="#">2</a></li>-->
        <!--                    <li class="page-item"><a class="page-link" href="#">3</a></li>-->
        <!--                    <li class="page-item"><a class="page-link" href="#">Next</a></li>-->
        <!--                </ul>-->
        <!--            </nav>-->
        <!--        </div>-->
    </div>
    <?php if (is_array($companies_info) && count($companies_info) >= 1) {
        foreach ($companies_info as $row) {
            ?>
            <div class="row">

                <div class="col-sm-3">
                    <div class="data-left">
                        <?php if ($row->logo == '') { ?>
                            <img class="img-responsive" src="<?php echo base_url() ?>assets/upload/company_logo.jpg"/>
                        <?php } else { ?>
                            <img class="img-responsive" src="<?php echo base_url() . $row->logo; ?>" height="200"
                                 width="200"/>
                        <?php } ?>
                        <ul>
                            <li><a href="<?php echo $row->web; ?>" target="_blank" class="btn btn-info">Web</a></li>
                            <li><a href="mailto:<?php echo $row->email; ?>" class="btn btn-info">Email</a></li>
                        </ul>

                    </div>
                </div>
                <div class="col-sm-9 container-fluid">


                    <table id="company_info" class="table table-striped">
                        <thead class="thead-inverse">
                        <tr>
                            <th class="info" width="30%"
                                style="background-color: #E5E5D8; color: black;border-radius:5px;">
                                Company Name
                            </th>
                            <th style="background-color: #F2E6AA; color: #592C16;border-radius:5px; height: 1px !important;">
                                <a href="<?php echo base_url() ?>welcome/company_details_info/<?php echo $row->branch_Id; ?>"><u
                                        style="color: #0000cc;"><?php echo $row->company_Name; ?></u>
                                    <?php
                                    if ($row->verification_status == 1)
                                    {
                                        echo '<img src="'.base_url().'download.png" width="30">';
                                    }
                                    ?>
                                </a>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr style="height: 5px;">
                            <td style="color: black;background-color: #E5E5D8 ;border-radius:5px;">
                                Business/Service Type
                            </td>
                            <td style="border-radius:5px; padding: 10px;;"><?php echo $row->service_type; ?></td>
                        </tr>
                        <tr>
                            <td style="color: black;background-color: #E5E5D8 ;border-radius:5px;">
                                Address
                            </td>
                            <td style="border-radius:5px; padding: 10px;;"><?php echo $row->address; ?></td>
                        </tr>
                        <tr>
                            <td style="color: black;background-color: #E5E5D8 ;border-radius:5px;">
                                Phone
                            </td>
                            <td style="border-radius:5px; padding: 10px;;"><?php echo $row->phone; ?></td>
                        </tr>

                        </tbody>
                    </table>
                    <a style="color: white" class="btn btn-primary pull-right"
                       href="<?php echo base_url() ?>welcome/company_details_info/<?php echo $row->branch_Id; ?>"><b
                            style="color: white">
                            Details</b></a>
                    <br/><br/>
                    <table class="">
                        <tr>
                            <td class="hrline" colspan="3"></td>
                        </tr>
                    </table>


                </div>

            </div>
        <?php }
    } else {
        echo '<h1 class="alert alert-info">No Data Found!</h1>';
    } ?>
    <div class="pull-right">

        <?php if ($companies_info != ''){

            echo '<p class="alert alert-info">Showing page '.$start_page.' of '.$total_page.', Total records '
                .$total_result.', Showing records '.$per_page.'.</p>';
        }?>
        <nav aria-label="Page navigation example">
            <?php
            foreach ($links as $link)
                echo $link;
            ?>
        </nav>
    </div>
</div>
<div class="col-md-2">
    <aside id="secondary" class="sidebar widget-area" role="complementary">
        <div id="text-5" class="sidebar widget_text">
            <div class="textwidget">
                <img src="http://placehold.it/180x200/bdc3c7/000000?text=Ad+Space">
                <img src="http://placehold.it/180x200/bdc3c7/000000?text=Ad+Space">
                <img src="http://placehold.it/180x200/bdc3c7/000000?text=Ad+Space">
                <img src="http://placehold.it/180x200/bdc3c7/000000?text=Ad+Space">
                <img src="http://placehold.it/180x200/bdc3c7/000000?text=Ad+Space">
            </div>
        </div>
    </aside>
</div>