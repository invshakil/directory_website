<div class="col-md-8 content-cat">
<div class="row">


    <div class="col-md-4">

        <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=17; echo $id;?>">
            <div class="left-content common-content">

                <figure style="background-color: #9b59b6">

                    <div></div>

                    <img class="img-responsive"
                         src="<?php echo base_url()?>wp-content/uploads/2016/12/Agriculture-225x210.jpg">

                    <figcaption>

                        <div class="img-content">

                            <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=17; echo $id;?>">Agriculture</a></h5>

                        </div>

                    </figcaption>

                </figure>

            </div>
        </a>

    </div>


    <div class="col-md-4">

        <div class="row">


            <div class="col-md-6">

                <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=45; echo $id;?>">
                    <div class="middle-content common-content three">

                        <figure style="background-color: #1DABB8">

                            <div></div>

                            <img class="img-responsive"
                                 src="<?php echo base_url()?>wp-content/uploads/2016/12/Shop-_-Retailers-102x93.jpg">

                            <figcaption>

                                <div class="img-content">

                                    <h5>
                                        <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=45; echo $id;?>"><br> Shop, Retailers &amp; Groceries</a></h5>

                                </div>

                            </figcaption>

                        </figure>

                    </div>
                </a>

            </div>


            <div class="col-md-6">

                <div class="middle-content common-content three">

                    <figure style="background-color: #8870FF">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Sports-_-Activites-102x93.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=64; echo $id;?>"> <br/>Sports &amp; Activites</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6">

                <div class="middle-content common-content three">

                    <figure style="background-color: #FF7416">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Suppliers-102x93.png">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=55; echo $id;?>">Suppliers</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6">

                <div class="middle-content common-content three">

                    <figure style="background-color: #D8335B">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Tailor-102x93.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=53; echo $id;?>">Tailor</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


        </div>

    </div>


    <div class="col-md-4">

        <div class="left-content common-content">

            <figure style="background-color: #2C82C9">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Textiles-_-Textile-Goods-225x210.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=35; echo $id;?>">Textiles &amp; Textile Goods</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>

    </div>


</div>

<div class="row">


    <div class="col-md-4">


        <div class="left-content common-content two">

            <figure style="background-color: #E3000E">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Transportation-Services-225x106.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=46; echo $id;?>">Transportation Services & Equipments</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>


        <div class="left-content common-content two">

            <figure style="background-color: #1abc9c">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Automobile-225x106.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=23; echo $id;?>">Auto Mobile</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>


    </div>


    <div class="col-md-4" style="padding-left: 0 !important; padding-right: 0 !important;">

        <div class="left-content common-content">

            <figure style="background-color: #8657DB">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Art-_-Culture-225x210.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=25; echo $id;?>">Art &amp; Culture</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>

    </div>
    <div class="col-md-4">


        <div class="left-content common-content two">

            <figure style="background-color: #953163">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/tools-225x106.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=65; echo $id;?>">Hardware tools</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>


        <div class="left-content common-content two">

            <figure style="background-color: #2980b9">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Banking-_-Insurance-225x106.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=4; echo $id;?>">Banking & Finance</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>
    </div>

</div>

<div class="row">

    <div class="col-md-4">

        <div class="row">


            <div class="col-md-6" style="padding-left: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #F62459">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2017/01/app_eng_siterunner-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=15; echo $id;?>">Building & Construction</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6" style="padding-right: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #1BA39C">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Computer-_-accessories-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=16; echo $id;?>">Computer & Accessories</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6" style="padding-left: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #48AD01">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2017/01/Printing-Publishing-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=49; echo $id;?>"> Printing  & Publishing</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6" style="padding-right: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #2C3E50">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2017/01/Corporate-Office-1-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=5; echo $id;?>">Corporate Office</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>

        </div>

    </div>


    <div class="col-md-4" style="padding-right:  0 !important; padding-left: 0 !important;">

        <div class="left-content common-content">

            <figure style="background-color: #E74C3C">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Education-OpportunitySmall-225x210.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=7; echo $id;?>">Education</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>

    </div>


    <div class="col-md-4">

        <div class="row">


            <div class="col-md-6" style="padding-left: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #26A65B">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Essentials-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=48; echo $id;?>">Essentials</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6" style="padding-right: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #F9690E">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Fuel-Station-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=47; echo $id;?>">Fuel Stations</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6" style="padding-left: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #663399">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Garments-_-Apparels-102x101.png">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=11; echo $id;?>">Garments &amp; Accessories</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6" style="padding-right: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #2C82C9">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Group-of-Companies-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=10; echo $id;?>">Group of Companies</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


        </div>

    </div>


</div>

<div class="row">


    <div class="col-md-4">

        <div class="left-content common-content">

            <figure style="background-color: #2980b9">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2017/01/Health-Fitness-225x210.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=39; echo $id;?>">Health & Fitness</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>

    </div>


    <div class="col-md-4" style="padding-left: 0 !important; padding-right:  0!important;">


        <div class="left-content common-content two">

            <figure style="background-color: #6F2480">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Rental-Services-225x98.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=62; echo $id;?>">Rental Services</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>


        <div class="left-content common-content two">

            <figure style="background-color: #e67e22">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Helpline-225x98.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=56; echo $id;?>">Helpline</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>
    </div>


    <div class="col-md-4">

        <div class="left-content common-content">

            <figure style="background-color: #60646D">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/fars-hotel-resorts-225x210.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=9; echo $id;?>">Hotel & Resorts</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>

    </div>


</div>


<div class="row">


    <div class="col-md-4">

        <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=57; echo $id;?>">
            <div class="left-content common-content">

                <figure style="background-color: #FF520F">

                    <div></div>

                    <img class="img-responsive"
                         src="<?php echo base_url()?>wp-content/uploads/2016/12/Business-Services-225x210.jpg">

                    <figcaption>

                        <div class="img-content">

                            <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=57; echo $id;?>">Business Service</a></h5>

                        </div>

                    </figcaption>

                </figure>

            </div>
        </a>

    </div>


    <div class="col-md-4">

        <div class="row">


            <div class="col-md-6">

                <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=29; echo $id;?>">
                    <div class="middle-content common-content three">

                        <figure style="background-color: #1BBC9B">

                            <div></div>

                            <img class="img-responsive"
                                 src="<?php echo base_url()?>wp-content/uploads/2017/01/House-Hold-Handicrafts-102x93.jpg">

                            <figcaption>

                                <div class="img-content">

                                    <h5>
                                        <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=29; echo $id;?>">House Hold &amp; Handicrafts</a></h5>

                                </div>

                            </figcaption>

                        </figure>

                    </div>
                </a>

            </div>


            <div class="col-md-6">

                <div class="middle-content common-content three">

                    <figure style="background-color: #953163">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Internate-Services-102x93.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=73; echo $id;?>">Internet Services</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6">

                <div class="middle-content common-content three">

                    <figure style="background-color: #2980b9">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2017/01/It-Telecommunication-102x93.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=14; echo $id;?>">IT/Telecommunication</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6">

                <div class="middle-content common-content three">

                    <figure style="background-color: #F62459">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/n64189591055_2012436_4525831-102x93.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=24; echo $id;?>">Weddings</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


        </div>

    </div>


    <div class="col-md-4">

        <div class="left-content common-content">

            <figure style="background-color: #2ECC71">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Travels-_-Tours-225x210.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=31; echo $id;?>">Travel & Tours</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>

    </div>


</div>

<div class="row">


    <div class="col-md-4">


        <div class="left-content common-content two">

            <figure style="background-color: #8C7E51">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Restaurent-_-Food-225x106.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=13; echo $id;?>">Restaurant, Food &amp; Beverage</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>


        <div class="left-content common-content two">

            <figure style="background-color: #F22613">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Legal-Services-225x106.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=61; echo $id;?>">Legal Services</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>


    </div>


    <div class="col-md-4" style="padding-left: 0 !important; padding-right: 0 !important;">

        <div class="left-content common-content">

            <figure style="background-color: #B17E22">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2017/01/Market-Shopping-Mall-225x210.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=44; echo $id;?>">Market &amp; Shopping Mall</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>

    </div>
    <div class="col-md-4">


        <div class="left-content common-content two">

            <figure style="background-color: #446CB3">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Mobile-Phones-_-Accessories-225x106.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=41; echo $id;?>">Mobile Phone & Accessories</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>


        <div class="left-content common-content two">

            <figure style="background-color: #FD5B03">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Pharmaceuticals-225x106.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=40; echo $id;?>">Pharmaceutical</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>
    </div>

</div>

<div class="row">

    <div class="col-md-4">

        <div class="row">


            <div class="col-md-6" style="padding-left: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #2C82C9">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/house-102x101.gif">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=6; echo $id;?>">Real State</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6" style="padding-right: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #26C281">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Doctors-1-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=8; echo $id;?>">Doctors</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6" style="padding-left: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #EB6361">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Industries-_-Manufacturer-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=22; echo $id;?>">Industries/Manufacturer</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6" style="padding-right: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #785EDD">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Event-Management-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=21; echo $id;?>">Event Management</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>

        </div>

    </div>


    <div class="col-md-4" style="padding-right:  0 !important; padding-left: 0 !important;">

        <div class="left-content common-content">

            <figure style="background-color: #F22613">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Furniture-_-Decoration-225x210.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=27; echo $id;?>">Furniture &amp; Decoration</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>

    </div>


    <div class="col-md-4">

        <div class="row">


            <div class="col-md-6" style="padding-left: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #663399">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Essentials-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=63; echo $id;?>">Repairing Services</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6" style="padding-right: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #E87E04">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2017/01/Media-Entertainment-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=18; echo $id;?>">Media & Entertainment</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6" style="padding-left: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #26A65B">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Consultancy-Services-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=59; echo $id;?>">Consultancy Services</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6" style="padding-right: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #D8335B">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Flower-Florist-Shop-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=68; echo $id;?>">Flower &amp; Florist Shop</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


        </div>

    </div>


</div>

<div class="row">


    <div class="col-md-4">

        <div class="left-content common-content">

            <figure style="background-color: #1DABB8">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/GlassThai-Aluminiums-225x210.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=69; echo $id;?>">Glass,Thai &amp; Aluminiums</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>

    </div>


    <div class="col-md-4" style="padding-left: 0 !important; padding-right:  0!important;">


        <div class="left-content common-content two">

            <figure style="background-color: #00D717">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Handyman-225x98.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=70; echo $id;?>">Handyman</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>


        <div class="left-content common-content two">

            <figure style="background-color: #27AE60">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Hobby-Classes-225x98.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=71; echo $id;?>">Hobby Classes</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>
    </div>


    <div class="col-md-4">

        <div class="left-content common-content">

            <figure style="background-color: #9E58DC">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Mobile-Phone-Recharge-Mobile-Banking-225x210.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=67; echo $id;?>">Mobile Recharge &amp; Mobile Banking</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>

    </div>


</div>


<div class="row">


    <div class="col-md-4">

        <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=20; echo $id;?>">
            <div class="left-content common-content">

                <figure style="background-color: #913D88">

                    <div></div>

                    <img class="img-responsive"
                         src="<?php echo base_url()?>wp-content/uploads/2016/12/Club-_-Association-225x210.jpg">

                    <figcaption>

                        <div class="img-content">

                            <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=20; echo $id;?>">Club &amp; Association</a></h5>

                        </div>

                    </figcaption>

                </figure>

            </div>
        </a>

    </div>


    <div class="col-md-4">

        <div class="row">


            <div class="col-md-6">

                <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=26; echo $id;?>">
                    <div class="middle-content common-content three">

                        <figure style="background-color: #282830">

                            <div></div>

                            <img class="img-responsive"
                                 src="<?php echo base_url()?>wp-content/uploads/2016/12/Books-_-Stationeries-102x93.jpg">

                            <figcaption>

                                <div class="img-content">

                                    <h5>
                                        <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=26; echo $id;?>">Books &amp; Stationeries</a></h5>

                                </div>

                            </figcaption>

                        </figure>

                    </div>
                </a>

            </div>


            <div class="col-md-6">

                <div class="middle-content common-content three">

                    <figure style="background-color: #48569E">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Chemical-_-Paper-102x93.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=32; echo $id;?>">Chemical &amp; Paper</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6">

                <div class="middle-content common-content three">

                    <figure style="background-color: #FD5B03">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Beauty-_-Spa-102x93.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=42; echo $id;?>">Beauty &amp; Spa</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6">

                <div class="middle-content common-content three">

                    <figure style="background-color: #0E9AA7">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Electrical-_-Electronics-102x93.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=50; echo $id;?>">Electrical &amp; Electronics</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


        </div>

    </div>


    <div class="col-md-4">

        <div class="left-content common-content">

            <figure style="background-color: #EE543A">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Emergency-Services-225x210.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=60; echo $id;?>">Emergency Services</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>

    </div>


</div>

<div class="row">


    <div class="col-md-4">


        <div class="left-content common-content two">

            <figure style="background-color: #FD5B03">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2017/01/Engineering-Workshop-225x106.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=51; echo $id;?>">Engineering Workshop</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>


        <div class="left-content common-content two">

            <figure style="background-color: #2CC990">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Food-_-Beverage-225x106.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=28; echo $id;?>">Newspaper, Tv Channels & Journalist</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>


    </div>


    <div class="col-md-4" style="padding-left: 0 !important; padding-right: 0 !important;">

        <div class="left-content common-content">

            <figure style="background-color: #D8335B">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Fashion-House-225x210.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=43; echo $id;?>">Fashion House</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>

    </div>
    <div class="col-md-4">


        <div class="left-content common-content two">

            <figure style="background-color: #6F2480">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Gems-_-Jewelleries-225x106.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=30; echo $id;?>">Gems &amp; Jewelleries</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>


        <div class="left-content common-content two">

            <figure style="background-color: #2C82C9">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Government-Organization-225x106.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=66; echo $id;?>">Government Organization</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>
    </div>

</div>

<div class="row">

    <div class="col-md-4">

        <div class="row">


            <div class="col-md-6" style="padding-left: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #897FBA">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/hospitals-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=12; echo $id;?>">Hospitals</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6" style="padding-right: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #2C82C9">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Jute-Goods-_-Tea-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=33; echo $id;?>">Jute Goods &amp; Tea</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6" style="padding-left: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #EB9532">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Leather-_-Leather-Goods-1-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=34; echo $id;?>">Leather &amp; Leather Goods</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6" style="padding-right: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #E01931">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Logistic-Services-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=58; echo $id;?>">Logistic Services</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>

        </div>

    </div>


    <div class="col-md-4" style="padding-right:  0 !important; padding-left: 0 !important;">

        <div class="left-content common-content">

            <figure style="background-color: #33A828">

                <div></div>

                <img class="img-responsive"
                     src="<?php echo base_url()?>wp-content/uploads/2016/12/Machinery-_-Equipment-225x210.jpg">

                <figcaption>

                    <div class="img-content">

                        <h5><a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=38; echo $id;?>">Machinery &amp; Equipment</a></h5>

                    </div>

                </figcaption>

            </figure>

        </div>

    </div>


    <div class="col-md-4">

        <div class="row">


            <div class="col-md-6" style="padding-left: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #FF7B2C">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Plastic-_-Rubber-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=37; echo $id;?>">Plastic &amp; Rubber</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6" style="padding-right: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #985004">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Servicing-Garage-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=52; echo $id;?>">Servicing &amp; Garage</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6" style="padding-left: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #1136C7">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Oil-_-Gas-Industries-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=36; echo $id;?>">Oil &amp; Gas Industries</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


            <div class="col-md-6" style="padding-right: 10px !important;">

                <div class="middle-content common-content three">

                    <figure style="background-color: #D8335B">

                        <div></div>

                        <img class="img-responsive"
                             src="<?php echo base_url()?>wp-content/uploads/2016/12/Groceries-102x101.jpg">

                        <figcaption>

                            <div class="img-content">

                                <h5>
                                    <a class="text-center"  href="<?php echo base_url();?>category_info/<?php $id=54; echo $id;?>"> Profile</a></h5>

                            </div>

                        </figcaption>

                    </figure>

                </div>

            </div>


        </div>

    </div>


</div>

<div class="row">


    <div class="col-md-4" style="padding-left: 0 !important; padding-right:  0!important;">


    </div>


</div>

</div>