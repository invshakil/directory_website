<div class="sidebar-menu">

    <div class="sidebar-menu-inner">

        <header class="logo-env">

            <!-- logo -->
            <div class="logo">
                <a href="<?php echo base_url();?>" target="_blank">
                    <img src="http://boichitrobd.mamalhospitality.com/techno-71.png" width="170" alt="" />
                </a>
            </div>

            <!-- logo collapse icon -->
            <div class="sidebar-collapse">
                <a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                    <i class="entypo-menu"></i>
                </a>
            </div>


            <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
            <div class="sidebar-mobile-menu visible-xs">
                <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                    <i class="entypo-menu"></i>
                </a>
            </div>

        </header>


        <ul id="main-menu" class="main-menu">
            <!-- add class "multiple-expanded" to allow multiple submenus to open -->
            <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

            <!-- DASHBOARD -->
            <li class="<?php if ($page_name == 'Admin Dashboard') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/index">
                    <i class="entypo-home"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <!-- CITY -->
            <li class="<?php if ($page_name == 'City') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/city">
                    <i class="entypo-archive"></i>
                    <span>City</span>
                </a>
            </li>

            <!-- Area -->
            <li class="<?php if ($page_name == 'Area') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/area">
                    <i class="entypo-air"></i>
                    <span>Area</span>
                </a>
            </li>

            <!-- Category -->
            <li class="<?php if ($page_name == 'Category') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/category">
                    <i class="entypo-adjust"></i>
                    <span>Category</span>
                </a>
            </li>

            <!-- SUBCategory -->
            <li class="<?php if ($page_name == 'Sub Category') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/subcategory">
                    <i class="entypo-block"></i>
                    <span>Sub Category</span>
                </a>
            </li>

            <!-- ADD COMPANY, Company Category, Branch -->
            <li class="<?php if ($page_name == 'Add Company') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/add_company">
                    <i class="entypo-eye"></i>
                    <span>Add Company</span>
                </a>
            </li>


            <li class="<?php if ($page_name == 'Add Company Category') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/company_category">
                    <i class="entypo-folder"></i>
                    <span>Add Company Category</span>
                </a>
            </li>

            <li class="<?php if ($page_name == 'Add Branch') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/branch">
                    <i class="entypo-info"></i>
                    <span>Add Branch</span>
                </a>
            </li>

            <!-- MANAGE COMPANY, BRANCH -->
            <li class="<?php if ($page_name == 'Company') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/manage_company">
                    <i class="entypo-eye"></i>
                    <span>Manage Company</span>
                </a>
            </li>

            <li class="<?php if ($page_name == 'Company Category') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/manage_company_category">
                    <i class="entypo-folder"></i>
                    <span>Manage Company Category</span>
                </a>
            </li>

            <li class="<?php if ($page_name == 'Manage Branch') echo 'active'; ?> ">
                <a href="<?php echo base_url(); ?>cms/manage_branch">
                    <i class="entypo-info"></i>
                    <span>Manage Branch</span>
                </a>
            </li>

            <li class="<?php if ($page_name == 'About Us' || $page_name == 'Advertise Info' || $page_name == 'Advertise with us'
                || $page_name == 'Contact' || $page_name == 'Feedback'
            ) echo 'opened active'; ?> ">
                <a href="#">
                    <i class="entypo-folder"></i> Pages
                </a>
                <ul>
                    <li class="<?php if ($page_name == 'About Us') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>cms/about_us">
                            <i class="entypo-info"></i>
                            <span>About Us</span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'Advertise Info') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>cms/advertise_info">
                            <i class="entypo-info"></i>
                            <span>Advertise Info</span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'Advertise with us') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>cms/advertise_with_us">
                            <i class="entypo-info"></i>
                            <span>Advertise With us</span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'Contact') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>cms/contact">
                            <i class="entypo-info"></i>
                            <span>Contact</span>
                        </a>
                    </li>
                    <li class="<?php if ($page_name == 'Feedback') echo 'active'; ?> ">
                        <a href="<?php echo base_url(); ?>cms/feedback">
                            <i class="entypo-info"></i>
                            <span>Feedback</span>
                        </a>
                    </li>

                </ul>
            </li>
        </ul>

    </div>

</div>