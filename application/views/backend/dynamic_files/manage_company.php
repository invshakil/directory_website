<div class="panel panel-dark" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">Company List</div>

        <div class="panel-options">
            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                    class="entypo-cog"></i></a>
            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
        </div>
    </div>

    <!-- panel body -->
    <div class="panel-body">

        <table class="table table-bordered datatable" id="table-1">
            <thead>
            <tr>
                <th data-hide="phone">ID</th>
                <th>Logo</th>
                <th>Company Name</th>
                <th>Member Status</th>
                <th>Status</th>
                <th>options</th>
            </tr>
            </thead>
            <tbody>
            <?php $info = $this->db->order_by('company_Id','desc')->get('tbl_company')->result();
            foreach ($info as $row) {
                ?>
                <tr class="odd gradeX">
                    <td><?php echo $row->company_Id; ?></td>
                    <td>
                        <?php
                        if ($row->logo != ''){ ?>
                            <img src="<?php echo base_url().$row->logo;?>" alt="" height="40" width="40">
                        <?php }else{
                            echo 'N/A';
                        }
                        ?>
                    </td>
                    <td><?php echo $row->company_Name; ?></td>
                    <td class="center"><?php if ($row->member_Status == 1) {
                            echo '<div class="label label-info">Yes</div>';
                        } else {
                            echo '<div class="label label-warning">No</div>';
                        } ?></td>
                    <td class="center"><?php if ($row->status == 1) {
                            echo '<div class="label label-success">published</div>';
                        } else {
                            echo '<div class="label label-danger">pending</div>';
                        } ?>
                    </td>

                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-info btn-sm dropdown-toggle"
                                    data-toggle="dropdown">
                                Action <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                <!-- EDITING LINK -->
                                <li>
                                    <a href="#"
                                       onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/modal_edit_company/<?php echo $row->company_Id; ?>');">
                                        <i class="entypo-pencil"></i>
                                        Edit
                                    </a>
                                </li>
                                <li class="divider"></li>

                                <!-- DELETION LINK -->
                                <li>
                                    <a href="#"
                                       onclick="confirm_modal('<?php echo base_url(); ?>cms/company/delete/<?php echo $row->company_Id; ?>');">
                                        <i class="entypo-trash"></i>
                                        Delete
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

</div>


<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function ($) {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,


            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>