<div class="panel panel-dark" data-collapsed="0">
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">Branch List</div>

        <div class="panel-options">
            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                    class="entypo-cog"></i></a>
            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
        </div>
    </div>

    <!-- panel body -->
    <div class="panel-body">

        <div class="row table-responsive">
            <table class="table table-bordered datatable" id="table-1">
                <thead>
                <tr>
                    <th width="3%">ID</th>
                    <th width="10%">Branch Name</th>
                    <th width="10%">Company Name</th>
                    <th width="10%">Service Type</th>
                    <th width="10%">Area</th>
                    <th width="17%">Address</th>
                    <th width="10%">Phone</th>
                    <th width="10%">options</th>
                </tr>
                </thead>
                <tbody>
                <?php $info = $this->db
                    ->select('c.company_Name, a.area_Name, b.*')
                    ->join('tbl_company c', 'c.company_Id = b.company_Id')
                    ->join('tbl_area a', 'a.area_Id = b.area_Id')
                    ->order_by('branch_Id', 'DESC')
                    ->get('tbl_branch b')
                    ->result();
                //                            echo '<pre>';print_r($info);exit;
                foreach ($info as $row) {
                    ?>
                    <tr class="odd gradeX">
                        <td><?php echo $row->branch_Id; ?></td>
                        <td><?php echo $row->branch_Name; ?></td>
                        <td><?php echo $row->company_Name; ?></td>
                        <td><?php echo $row->service_type; ?></td>
                        <td><?php echo $row->area_Name; ?></td>
                        <td><?php echo $row->address; ?></td>
                        <td><?php echo $row->phone; ?></td>

                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-info btn-sm dropdown-toggle"
                                        data-toggle="dropdown">
                                    Action <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                    <!-- EDITING LINK -->
                                    <li>
                                        <a href="#"
                                           onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/modal_edit_branch/<?php echo $row->branch_Id; ?>');">
                                            <i class="entypo-pencil"></i>
                                            Edit
                                        </a>
                                    </li>
                                    <li class="divider"></li>

                                    <!-- DELETION LINK -->
                                    <li>
                                        <a href="#"
                                           onclick="confirm_modal('<?php echo base_url(); ?>cms/branch/delete/<?php echo $row->branch_Id; ?>');">
                                            <i class="entypo-trash"></i>
                                            Delete
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

</div>


<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function ($) {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,


            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>