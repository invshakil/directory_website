<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel-heading">
        <div class="panel-title"><h3>Add / Manage Subcategory</h3></div>
        <div class="panel-options">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#profile-1" data-toggle="tab">Sub Category List</a></li>
                <li><a href="#profile-2" data-toggle="tab">Add Sub Category</a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="tab-content">
            <div class="tab-pane active" id="profile-1">

                <div class="panel panel-dark" data-collapsed="0">
                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Sub Category List</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">

                        <table class="table table-bordered datatable" id="table-1">
                            <thead>
                            <tr>
                                <th data-hide="phone">ID</th>
                                <th>Sub Category Name</th>
                                <th>City</th>
                                <th>Status</th>
                                <th>options</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $info = $this->db->select()->join('tbl_category a', 'a.category_Id = d.category_Id')->get('tbl_subcategory d')->result();

                            foreach ($info as $row) {
                                ?>
                                <tr class="odd gradeX">
                                    <td><?php echo $row->sub_Category_Id; ?></td>
                                    <td><?php echo $row->sub_Category_Name; ?></td>
                                    <td><?php echo $row->category_Name; ?></td>
                                    <td class="center"><?php if ($row->s_status == 1) {
                                            echo '<div class="label label-success">published</div>';
                                        } else {
                                            echo '<div class="label label-danger">pending</div>';
                                        } ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info btn-sm dropdown-toggle"
                                                    data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                <!-- EDITING LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/modal_edit_subcategory/<?php echo $row->sub_Category_Id; ?>');">
                                                        <i class="entypo-pencil"></i>
                                                        Edit
                                                    </a>
                                                </li>
                                                <li class="divider"></li>

                                                <!-- DELETION LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="confirm_modal('<?php echo base_url(); ?>cms/subcategory/delete/<?php echo $row->sub_Category_Id; ?>');">
                                                        <i class="entypo-trash"></i>
                                                        Delete
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

            <div class="tab-pane" id="profile-2">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Add Subcategory</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">

                        <form role="form" class="form-horizontal form-groups-bordered" method="post"
                              action="<?php echo base_url() ?>cms/subcategory/create">

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Subcategory Name</label>

                                <div class="col-sm-5">
                                    <input type="text" name="sub_Category_Name" class="form-control" id="field-1"
                                           placeholder="Enter Sub Category Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Category</label>

                                <div class="col-sm-5">
                                    <select name="category_Id" class="form-control">
                                        <option value="1">Select</option>
                                        <?php $cat = $this->db->order_by('category_Name', 'asc')->get('tbl_category')->result();
                                        foreach ($cat as $row) {
                                            ?>
                                            <option
                                                value="<?php echo $row->category_Id ?>"><?php echo $row->category_Name ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Status</label>

                                <div class="col-sm-5">
                                    <select name="s_status" class="form-control">
                                        <option value="1">Published</option>
                                        <option value="0">Pending</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" class="btn btn-success">Add Sub Category</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>

            </div>
        </div>

    </div>


</div>

<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function ($) {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,


            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>