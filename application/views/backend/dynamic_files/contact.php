<div class="panel panel-dark" data-collapsed="0">
    <?php
    $this->session->flashdata('message');
    ?>
    <!-- panel head -->
    <div class="panel-heading">
        <div class="panel-title">About Us</div>

        <div class="panel-options">
            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                    class="entypo-cog"></i></a>
            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
        </div>
    </div>

    <!-- panel body -->
    <div class="panel-body">

        <form role="form" class="form-horizontal form-groups-bordered" method="post"
              action="<?php echo base_url() ?>cms/contact/create" enctype="multipart/form-data">

            <div class="form-group">
                <label for="field-1" class="col-sm-1 control-label">Description</label>

                <div class="col-sm-11">
                   <textarea type="text" name="contact" id="summernote" class="form-control"
                             placeholder="Enter Description"><?php
                       $about_us = $this->db->get_where('frontend_descriptions',
                           array('info_type' => 'contact'))->row('description');
                       echo $about_us;
                       ?></textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-5">
                    <button type="submit" class="btn btn-success">Save Description</button>
                </div>
            </div>
        </form>

    </div>
</div>

<script src="https://code.jquery.com/jquery-1.8.0.min.js"></script>

<!-- include summernote css/js-->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>

<script>
    $(document).ready(function () {
        $('#summernote').summernote({
            height: 400,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: true                  // set focus to editable area after initializing summernote
        });
    });
</script>