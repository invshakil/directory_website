<?php
$edit_data = $this->db->get_where('tbl_company', array('company_Id' => $param2))->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach ($edit_data as $row): ?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>cms/company/do_update/<?php echo $row['company_Id'] ?>"
                  enctype="multipart/form-data">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Company Name</label>

                    <div class="col-sm-5">
                        <input type="text" name="company_Name" class="form-control"
                               value="<?php echo $row['company_Name'] ?>" id="field-1" placeholder="Enter Company Name">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Logo Upload</label>

                    <div class="col-sm-5">

                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"
                                 data-trigger="fileinput">
                                <?php if ($row['logo'] != '') { ?>
                                    <img src="<?php echo base_url() . $row['logo'] ?>" alt="...">
                                <?php } else { ?>
                                    <img src="http://placehold.it/200x150" alt="...">
                                <?php } ?>
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px"></div>
                            <div>
											<span class="btn btn-white btn-file">
                                                <?php if ($row['logo'] != '') { ?>
                                                    <span class="fileinput-new">Select New Image</span>
                                                <?php } else { ?>
                                                    <span class="fileinput-new">Select Image</span>
                                                <?php } ?>
                                                <span class="fileinput-exists">Change</span>
												<input type="file" name="logo" accept="image/*">
											</span>
                                <a href="#" class="btn btn-orange fileinput-exists"
                                   data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Membership Status</label>

                    <div class="col-sm-5">
                        <select name="member_Status" class="form-control">
                            <option value="1" <?php if ($row['member_Status'] == 1) echo 'selected'; ?>
                            >Yes
                            </option>
                            <option value="0"<?php if ($row['member_Status'] == 0) echo 'selected'; ?>
                            >No
                            </option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Status</label>

                    <div class="col-sm-5">
                        <select name="status" class="form-control">
                            <option value="1" <?php if ($row['status'] == 1) echo 'selected'; ?>
                            >Published
                            </option>
                            <option value="0"<?php if ($row['status'] == 0) echo 'selected'; ?>
                            >Pending
                            </option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Company Info</button>
                    </div>
                </div>

            </form>
        <?php endforeach; ?>
    </div>
</div>