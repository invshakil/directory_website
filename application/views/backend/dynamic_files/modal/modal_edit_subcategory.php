<?php
$edit_data		=	$this->db->get_where('tbl_subcategory' , array('sub_Category_Id' => $param2) )->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach($edit_data as $row):?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>cms/subcategory/do_update/<?php echo $row['sub_Category_Id']?>">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Area Name</label>

                    <div class="col-sm-5">
                        <input type="text" name="sub_Category_Name" class="form-control" value="<?php echo $row['sub_Category_Name']?>" id="field-1" placeholder="Enter Sub Category Name">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Category</label>

                    <div class="col-sm-5">
                        <select name="category_Id" class="form-control">
                            <option>Select</option>
                            <?php $city = $this->db->get('tbl_category')->result_array();
                            foreach ($city as $row1) {?>
                                <option value="<?php echo $row1['category_Id']?>" <?php if($row['category_Id'] == $row1['category_Id'] ) echo 'selected';?>
                                ><?php echo $row1['category_Name']?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Status</label>

                    <div class="col-sm-5">
                        <select name="s_status" class="form-control">
                            <option value="1" <?php if($row['s_status'] ==1 ) echo 'selected';?>
                            >Published</option>
                            <option value="0"<?php if($row['s_status'] ==0 ) echo 'selected';?>
                            >Pending</option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Sub Category</button>
                    </div>
                </div>

            </form>
        <?php endforeach;?>
    </div>
</div>