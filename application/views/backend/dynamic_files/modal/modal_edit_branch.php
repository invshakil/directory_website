<?php
$edit_data		=	$this->db->join('tbl_area a','a.area_Id = b.area_Id')->get_where('tbl_branch b' , array('b.branch_Id' => $param2) )->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach($edit_data as $row):?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>cms/branch/do_update/<?php echo $row['branch_Id']?>">

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Select Company</label>

                    <div class="col-sm-5">
                        <select name="company_Id" class="form-control">

                                <?php $company = $this->db->get('tbl_company')->result_array();
                                foreach ($company as $r) {?>
                                    <option value="<?php echo $r['company_Id'];?>"
                                    <?php if ($r['company_Id']==$row['company_Id']) echo 'selected';?>
                                    ><?php echo $r['company_Name'];?>
                                    </option>

                                <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Select City</label>

                    <div class="col-sm-5">
                        <select name="city_Id" class="form-control">

                            <?php $city = $this->db->order_by('city_Name','asc')->get('tbl_city')->result_array();
                            foreach ($city as $r) {?>
                                <option value="<?php echo $r['city_Id'];?>"
                                <?php if ($r['city_Id'] == $row['city_Id']) echo 'selected';?>
                                ><?php echo $r['city_Name'];?></option>

                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Area</label>
                    <div class="col-sm-5">
                        <select name="area_Id" class="form-control">
                            <?php $area = $this->db->order_by('area_Name','asc')->get_where('tbl_area', array('city_Id'=>$row['city_Id']))->result_array();
                            foreach ($area as $r) {?>
                                <option value="<?php echo $r['area_Id'];?>"
                                    <?php if ($r['area_Id'] == $row['area_Id']) echo 'selected';?>
                                ><?php echo $r['area_Name'];?></option>

                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Branch Name</label>

                    <div class="col-sm-5">
                        <input type="text" name="branch_Name" value="<?php echo $row['branch_Name'];?>" class="form-control" id="field-1" placeholder="Enter Branch Name">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Service Type</label>

                    <div class="col-sm-5">
                        <input type="text" name="service_type" value="<?php echo $row['service_type'];?>" class="form-control" id="field-1" placeholder="Enter Service Type">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label">Address</label>

                    <div class="col-sm-5">
                        <textarea class="form-control" name="address" id="field-ta" placeholder="Enter Address of this branch"><?php echo $row['address'];?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label">Phone</label>

                    <div class="col-sm-5">
                        <textarea class="form-control" name="phone" id="field-ta" placeholder="Enter Phone number of this branch"><?php echo $row['phone'];?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Web Address</label>

                    <div class="col-sm-5">
                        <input type="text" name="web" value="<?php echo $row['web'];?>" class="form-control" id="field-1" placeholder="Enter Web Address">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Meta Name</label>

                    <div class="col-sm-5">
                        <input type="text" name="meta_Name" value="<?php echo $row['meta_Name'];?>" class="form-control" id="field-1" placeholder="Enter Meta Name">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label">Meta Keyword</label>

                    <div class="col-sm-5">
                        <textarea class="form-control" name="meta_Keyword" id="field-ta" placeholder="Enter Meta Keyword"><?php echo $row['meta_Keyword'];?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Email</label>

                    <div class="col-sm-5">
                        <input type="email" name="email" value="<?php echo $row['email'];?>" class="form-control" id="field-1" placeholder="Enter Email Address">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Select Verification Status</label>

                    <div class="col-sm-5">
                        <select name="verification_status" class="form-control">
                            <option value="0" <?php if($row['verification_status'] ==0 ) echo 'selected';?>>Not Verified</option>
                            <option value="1" <?php if($row['verification_status'] ==1 ) echo 'selected';?>>Verified</option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Branch Information</button>
                    </div>
                </div>

            </form>
        <?php endforeach;?>
    </div>
</div>