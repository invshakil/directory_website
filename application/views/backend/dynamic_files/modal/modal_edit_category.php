<?php
$edit_data = $this->db->get_where('tbl_category', array('category_Id' => $param2))->result_array();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach ($edit_data as $row): ?>
            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>cms/category/do_update/<?php echo $row['category_Id'] ?>">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Category Name</label>

                    <div class="col-sm-5">
                        <input type="text" name="category_Name" class="form-control" value="<?php echo $row['category_Name'] ?>"
                               id="field-1" placeholder="Enter category Name">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Status</label>

                    <div class="col-sm-5">
                        <select name="status" class="form-control">
                            <option value="1" <?php if ($row['status'] == 1) echo 'selected'; ?>
                            >Published
                            </option>
                            <option value="0"<?php if ($row['status'] == 0) echo 'selected'; ?>
                            >Pending
                            </option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Category</button>
                    </div>
                </div>

            </form>
        <?php endforeach; ?>
    </div>
</div>