<?php
$edit_data = $this->db->get_where('tbl_company_category', array('id' => $param2))->result_array();


?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach ($edit_data as $row): ?>

            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="<?php echo base_url() ?>cms/company_category/do_update/<?php echo $row['id'] ?>">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Company Name</label>
                    <?php $name = $this->db->get_where('tbl_company', array('company_Id' => $row['company_Id']))->row('company_Name'); ?>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" value="<?php echo $name ?>" id="field-1" disabled>
                        <input type="hidden" name="company_Id" class="form-control"
                               value="<?php echo $row['company_Id'] ?>" id="field-1">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"> Category</label>

                    <div class="col-sm-5">
                        <?php $c_name = $this->db->get_where('tbl_category', array('category_Id' => $row['category_Id']))->row('category_Name'); ?>

                        <input type="text" class="form-control" value="<?php echo $c_name ?>" id="field-1" disabled>
                        <input type="hidden" name="category_Id" class="form-control"
                               value="<?php echo $row['category_Id'] ?>" id="field-1">

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Select Sub Category</label>

                    <div class="col-sm-5">
                        <select name="sub_Category_Id" class="form-control">
                            <?php $sub = $this->db->get_where('tbl_subcategory',array('category_Id'=>$row['category_Id']))->result_array();

                            foreach ($sub as $r) {
                                ?>
                                <option
                                    value="<?php echo $r['sub_Category_Id'] ?>"
                                <?php if($r['sub_Category_Id']==$row['sub_Category_Id']) echo 'selected';?>
                                ><?php echo $r['sub_Category_Name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary">Update Company Category</button>
                    </div>
                </div>

            </form>
        <?php endforeach; ?>
    </div>
</div>