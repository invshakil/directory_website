<div class="panel minimal minimal-gray">
    <?php
    $this->session->flashdata('message');
    ?>

    <div class="panel-heading">
        <div class="panel-title"><h3>Add Branch</h3></div>
        <div class="panel-options">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#profile-1" data-toggle="tab">Branch List</a></li>
                <li><a href="#profile-2" data-toggle="tab">Add Branch</a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">

        <div class="tab-content">
            <div class="tab-pane active" id="profile-1">

                <div class="panel panel-dark" data-collapsed="0">
                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Branch List</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">

                        <table class="table table-bordered datatable" id="table-1">
                            <thead>
                            <tr>
                                <th data-hide="phone">ID</th>
                                <th>Branch Name</th>
                                <th>Company Name</th>
                                <th>Service Type</th>
                                <th>Area</th>
                                <th>Address</th>
                                <th>Phone</th>
                                <th>options</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $info = $this->db
                                ->join('tbl_company c', 'c.company_Id = b.company_Id')
                                ->join('tbl_area a', 'a.area_Id = b.area_Id')
                                ->order_by('branch_Id','DESC')
                                ->limit(10)
                                ->get('tbl_branch b')
                                ->result();
//                            echo '<pre>';print_r($info);exit;
                            foreach ($info as $row) {
                                ?>
                                <tr class="odd gradeX">
                                    <td><?php echo $row->branch_Id; ?></td>
                                    <td><?php echo $row->branch_Name; ?></td>
                                    <td><?php echo $row->company_Name; ?></td>
                                    <td><?php echo $row->service_type; ?></td>
                                    <td><?php echo $row->area_Name; ?></td>
                                    <td><?php echo $row->address; ?></td>
                                    <td><?php echo $row->phone; ?></td>

                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info btn-sm dropdown-toggle"
                                                    data-toggle="dropdown">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                <!-- EDITING LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/modal_edit_branch/<?php echo $row->branch_Id; ?>');">
                                                        <i class="entypo-pencil"></i>
                                                        Edit
                                                    </a>
                                                </li>
                                                <li class="divider"></li>

                                                <!-- DELETION LINK -->
                                                <li>
                                                    <a href="#"
                                                       onclick="confirm_modal('<?php echo base_url(); ?>cms/branch/delete/<?php echo $row->branch_Id; ?>');">
                                                        <i class="entypo-trash"></i>
                                                        Delete
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

            <div class="tab-pane" id="profile-2">

                <div class="panel panel-dark" data-collapsed="0">

                    <!-- panel head -->
                    <div class="panel-heading">
                        <div class="panel-title">Add Area</div>

                        <div class="panel-options">
                            <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                    class="entypo-cog"></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <!-- panel body -->
                    <div class="panel-body">

                        <form role="form" class="form-horizontal form-groups-bordered" method="post"
                              action="<?php echo base_url() ?>cms/branch/create">

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Select Company</label>

                                <div class="col-sm-5">
                                    <select name="company_Id" class="select2" data-allow-clear="true" data-placeholder="Select one company...">
                                        <option></option>
                                        <optgroup label="Company List">
                                            <?php $company = $this->db->get('tbl_company')->result_array();
                                            foreach ($company as $r) {?>
                                                <option value="<?php echo $r['company_Id'];?>"><?php echo $r['company_Name'];?></option>

                                            <?php } ?>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-ta" class="col-sm-3 control-label">Address</label>

                                <div class="col-sm-5">
                                    <textarea class="form-control" name="address" id="field-ta" placeholder="Enter Address of this branch"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Select City</label>

                                <div class="col-sm-5">
                                    <select name="city_Id" class="form-control">

                                            <?php $city = $this->db->order_by('city_Name','asc')->get('tbl_city')->result_array();
                                            foreach ($city as $r) {?>
                                                <option value="<?php echo $r['city_Id'];?>"><?php echo $r['city_Name'];?></option>

                                            <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Area/Thana</label>
                                <div class="col-sm-5">
                                    <select name="area_Id" class="form-control">


                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Branch Name</label>

                                <div class="col-sm-5">
                                    <input type="text" name="branch_Name" class="form-control" id="field-1" placeholder="Enter Branch Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Business/Service Type</label>

                                <div class="col-sm-5">
                                    <input type="text" name="service_type" class="form-control" id="field-1" placeholder="Enter Business/Service Type">
                                </div>
                            </div>



                            <div class="form-group">
                                <label for="field-ta" class="col-sm-3 control-label">Phone Number</label>

                                <div class="col-sm-5">
                                    <textarea class="form-control" name="phone" id="field-ta" placeholder="Enter Phone number of this branch"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Email</label>

                                <div class="col-sm-5">
                                    <input type="email" name="email" class="form-control" id="field-1" placeholder="Enter Email Address">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Web Address</label>

                                <div class="col-sm-5">
                                    <input type="text" name="web" class="form-control" id="field-1" placeholder="Enter Web Address">
                                    <br/>
                                    <span class="label label-danger">Add http:// in url</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Meta Name</label>

                                <div class="col-sm-5">
                                    <input type="text" name="meta_Name" class="form-control" id="field-1" placeholder="Enter Meta Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-ta" class="col-sm-3 control-label">Meta Keyword</label>

                                <div class="col-sm-5">
                                    <textarea class="form-control" name="meta_Keyword" id="field-ta" placeholder="Enter Meta Keyword"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Select Verification Status</label>

                                <div class="col-sm-5">
                                    <select name="verification_status" class="form-control">
                                            <option value="0">Not Verified</option>
                                            <option value="1">Verified</option>
                                    </select>
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" class="btn btn-success">Add Branch</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>

            </div>
        </div>

    </div>


</div>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('select[name="city_Id"]').on('change', function () {
            var city_Id = $(this).val();
            console.log(city_Id);

            $.ajax({
                url: '<?php echo base_url()?>cms/get_area_id/' + city_Id,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('select[name="area_Id"]').empty();
                    $.each(data, function (key, value) {
                        $('select[name="area_Id"]').append('<option value="' + value.area_Id + '">' + value.area_Name + '</option>');
                    });
                }
            });

        });
    });
</script>

<script type="text/javascript">
    var responsiveHelper;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };
    var tableContainer;

    jQuery(document).ready(function ($) {
        tableContainer = $("#table-1");

        tableContainer.dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true,


            // Responsive Settings
            bAutoWidth: false,
            fnPreDrawCallback: function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper) {
                    responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                }
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                responsiveHelper.createExpandIcon(nRow);
            },
            fnDrawCallback: function (oSettings) {
                responsiveHelper.respond();
            }
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
</script>